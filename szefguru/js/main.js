/*
 * console.log style
 * awesome stuff...as always
 */
var log_style_start = [
    'background-color: #222',
    'color: orange',
    'display: inline-block',
    'border-left: 5px solid orange',
    'border-radius: 0 12px 12px 0',
    'padding: 5px',
    'line-height: 24px',
    'font-weight: bold'
].join(';');

var log_style_done = [
    'background-color: #222',
    'color: lime',
    'display: inline-block',
    'border-left: 5px solid lime',
    'border-radius: 0 12px 12px 0',
    'padding: 5px',
    'line-height: 24px',
    'font-weight: bold'
].join(';');

console.log('%c main loading started... ', log_style_start);

var baseUrl = 'css/';

function includeCss(cssFile, baseUrl) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    if (baseUrl === undefined) {
        link.href = cssFile;
    }
    else {
        link.href = baseUrl + cssFile + '?rand=' + Math.random();
    }
    link.media = 'all';
    head.appendChild(link);
}

/*
 * include js files with randoms cuz we don't like cached files...
 */
window.onload = function() {
    function includeJs(jsFile, baseUrl) {
        var jsNode = document.createElement("script");
        jsNode.type = "text/javascript";
        jsNode.src = baseUrl + jsFile;
        if (baseUrl === undefined) {
            jsNode.src = jsFile;
        }
        else {
            jsNode.src = baseUrl + jsFile + '?rand=' + Math.random();
        }
        jsNode.async = false;
        document.body.appendChild(jsNode);
    }

    includeJs('bootstrap.js', 'lib/bootstrap/js/');
    includeJs('functions.js', 'js/');
}

/*
 * include css files...
 */
includeCss('bootstrap.css', 'lib/bootstrap/css/');
includeCss('mfglabs_iconset.css', 'lib/mfglabs-iconset-master/css/');
includeCss('main.css', baseUrl);

console.log('%c main loaded. ', log_style_done);