/*
 * big buttons...
 * meh...close enough
 */

var button = $(".tile #button");

button.click(function() {
	var data = $(this).attr('data-name');
	$("article").slideUp();

	$("article#" + data).slideToggle();

	//console.log(data);

});

/*
 * Call carousel manually with 5sec delay and cycle
 */

$('.carousel').carousel({
	interval: 5000
})

$('.carousel').carousel('cycle');